import { Component } from '@angular/core';
import { TablaData } from './interfaces/tabla.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  ELEMENT_DATA: TablaData = {
    headers: ['Name', 'Salary', 'Age'],
    data: [],
  };
}
