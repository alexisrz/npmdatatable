import { HttpClient } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { TablaData } from 'src/app/interfaces/tabla.interface';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss'],
})
export class DatatableComponent implements OnDestroy, OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject<any>();
  @Input() displayData!: TablaData;

  columns: String[] = [];
  dataSource!: any;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,

      language: {
        url: '//cdn.datatables.net/plug-ins/1.11.3/i18n/es-mx.json',
      },
    };
    this.loadData();
  }

  loadData() {
    this.columns = this.displayData.headers;
    console.log('Columns ', this.columns);

    this.http
      .get('http://dummy.restapiexample.com/api/v1/employees')
      .subscribe((res: any) => {
        this.dataSource = res.data;
        console.log('dataSource', this.dataSource);
        this.dtTrigger.next();
      });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
}
